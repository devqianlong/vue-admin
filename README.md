# vue-admin

基于thinkphp5、vue、element-ui而实现的前后端分离完整解决方案

vue代码位于/public/vue-admin-code/下，使用vue-cli3脚手架

感谢技术栈:

1、vue2 

2、element-ui2.4

2、vue-element-admin

3.thinkphp5


 **【好消息】基于TP6+Vue3+Antdv2.1而实现的前后分离的管理系统已上线，目前对外提供企业授权版（内置微信支付、支付宝支付、阿里云OSS存储、阿里短信、Email、广告管理、文章管理等功能，同时，采用RBAC，实现按钮级权限控制），欢迎咨询：QQ：87989431** 